from django.db import models

class Categoria(models.Model):
    nombre = models.CharField(max_length=30)
    categoria_padre = models.ForeignKey("Categoria",on_delete=models.SET_NULL,null=True,blank=True)

    def __unicode__(self):
        return self.nombre

class Marca(models.Model):
    nombre = models.CharField(max_length=30)
    imagen = models.URLField(null=True,blank=True)

    def __unicode__(self):
        return self.nombre

class Producto(models.Model):
    codigo = models.CharField(max_length=30)
    ean = models.CharField(max_length=15,null=True,blank=True)
    nombre = models.CharField(max_length=200)
    precio = models.DecimalField(max_digits=11,decimal_places=2)
    activo = models.BooleanField()
    cantidad = models.IntegerField(null=True,blank=True)
    foto1 = models.URLField(null=True,blank=True)
    foto2 = models.URLField(null=True,blank=True)
    foto3 = models.URLField(null=True,blank=True)
    foto4 = models.URLField(null=True,blank=True)
    foto5 = models.URLField(null=True,blank=True)
    foto6 = models.URLField(null=True,blank=True)
    categoria = models.ForeignKey("Categoria",on_delete=models.SET_NULL,null=True,blank=True)
    marca = models.ForeignKey("Marca",on_delete=models.SET_NULL,null=True,blank=True)
    descripcion = models.TextField(null=True,blank=True)

    def __unicode__(self):
        return self.nombre
