from django_summernote.widgets import SummernoteWidget, SummernoteInplaceWidget
from django import forms

class Summernote(forms.Form):
    summernote = forms.CharField(widget=SummernoteInplaceWidget(attrs={"width":'100%'}))
