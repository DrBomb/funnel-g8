from __future__ import absolute_import
from django.conf import settings
from celery import shared_task
from .models import Producto, Marca
from resizeimage import resizeimage
from PIL import Image
import boto3, re
import os
client = boto3.resource(
        's3',
        aws_access_key_id=settings.AWS_S3['ACCESS_KEY'],
        aws_secret_access_key=settings.AWS_S3['SECRET_KEY']
)

@shared_task
def update_url(url,product_id,field):
    product = Producto.objects.get(id=product_id)
    exec('product.foto{} = "{}"'.format(field,url))
    print "ID: {}, Field: {}".format(product_id,field)
    product.save()

@shared_task
def update_url_brand(url,brand_id):
    brand = Marca.objects.get(id=brand_id)
    exec('brand.imagen = "{}"'.format(url))
    print "Brand: {} Subido".format(brand_id)
    brand.save()

@shared_task
def upload_image(path,product_id,field,uuid,ext):
    filename = "{}{}".format(uuid,ext)
    thumb_filename = "{}.jpg".format(uuid)
    client.Bucket(settings.AWS_S3['IMAGES_BUCKET']).put_object(Key=filename,Body=open(path,'rb'))
    print filename," subido"
    with open(path,'r+b') as f:
        try:
            with Image.open(f) as image:
                thumbnail = resizeimage.resize_thumbnail(image,[settings.THUMBNAIL_WIDTH,settings.THUMBNAIL_WIDTH])
                thumbnail.save(path+"thumb.jpg",format='JPEG')
        except:
            print "ThumbnailError"
    client.Bucket(settings.AWS_S3['THUMBNAILS_BUCKET']).put_object(Key=thumb_filename,Body=open(path+"thumb.jpg",'rb'))
    print thumb_filename," thumb subido"
    url = "{}/{}/{}".format(settings.AWS_S3["BASE_URL"],settings.AWS_S3['IMAGES_BUCKET'],filename)
    update_url.delay(url,product_id,field)
    os.remove(path)
    os.remove(path+"thumb.jpg")

@shared_task
def remove_image(deleted_key,ext):
    client.Object(settings.AWS_S3['IMAGES_BUCKET'],deleted_key+ext).delete()
    client.Object(settings.AWS_S3['THUMBNAILS_BUCKET'],deleted_key+".jpg").delete()
    print deleted_key + " Borrada"

@shared_task
def upload_image2(path,brand_id,uuid,ext):
    filename = "{}{}".format(uuid,ext)
    thumb_filename = "{}.jpg".format(uuid)
    client.Bucket(settings.AWS_S3['IMAGES_BUCKET']).put_object(Key=filename,Body=open(path,'rb'))
    print filename," subido"
    with open(path,'r+b') as f:
        try:
            with Image.open(f) as image:
                thumbnail = resizeimage.resize_thumbnail(image,[settings.THUMBNAIL_WIDTH,settings.THUMBNAIL_WIDTH])
                thumbnail.save(path+"thumb.jpg",format='JPEG')
        except:
            print "ThumbnailError"
    client.Bucket(settings.AWS_S3['THUMBNAILS_BUCKET']).put_object(Key=thumb_filename,Body=open(path+"thumb.jpg",'rb'))
    print thumb_filename," thumb subido"
    url = "{}/{}/{}".format(settings.AWS_S3["BASE_URL"],settings.AWS_S3['IMAGES_BUCKET'],filename)
    update_url_brand.delay(url,brand_id)
    os.remove(path)
    os.remove(path+"thumb.jpg")
