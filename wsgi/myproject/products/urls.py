from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$',views.index,name="index"),
    url(r'^create/{0,1}$',views.create_product,name="create_product"),
    url(r'^view/(\d+)$',views.view_product,name="view_product"),
    url(r'^edit/(\d*)$',views.edit_product,name="edit_product"),
    url(r'^delete/(\d+)$',views.delete_product,name="delete_product"),
    url(r'^brands/{0,1}$',views.list_brands,name="list_brands"),
    url(r'^brands/view/(\d+)$',views.view_brand,name="view_brand"),
    url(r'^brands/create$',views.create_brand,name="create_brand"),
    url(r'^brands/edit/(\d*)$',views.edit_brand,name="edit_brand"),
    url(r'^tags/{0,1}$',views.list_tags,name="list_tags"),
    url(r'^tags/create$',views.create_tag,name="create_tag"),
    url(r'^tags/view/(\d+)$',views.view_tag,name="view_tag"),
    url(r'^tags/edit/(\d*)$',views.edit_tag,name="edit_tag"),
]
