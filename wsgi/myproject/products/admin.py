from django.contrib import admin
from .models import Categoria,Marca,Producto

admin.site.register(Producto)
admin.site.register(Marca)
admin.site.register(Categoria)
