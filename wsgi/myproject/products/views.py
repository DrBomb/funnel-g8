# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.contrib import messages
from django.conf import settings
from .tasks import upload_image, remove_image, upload_image2
from .models import Producto,Marca,Categoria
from .forms import Summernote
from uuid import uuid4
import re, pdb

@login_required
def index(request):
    summernote = Summernote()
    productos = Producto.objects.all()
    marcas = Marca.objects.all()
    categorias = Categoria.objects.all()
    tag_filter = request.GET.getlist('f1')
    brand_filter = request.GET.getlist('f2')
    if len(tag_filter)>0:
        old = productos
        productos = []
        for x in old:
            try:
                if str(x.categoria.id) in tag_filter:
                    productos.append(x)
            except:
                pass
    if len(brand_filter)>0:
        old = productos
        productos = []
        for x in old:
            try:
                if str(x.marca.id) in brand_filter:
                    productos.append(x)
            except:
                pass
    return render(request, 'product_list.html', {'productos': productos,'marcas':marcas,'categorias':categorias,'tag_filter':tag_filter,'brand_filter':brand_filter,'summernote':summernote})

def upload_image_task(image,id,field):
    ext = re.match('(.*?)(\.[^.]*$|$)',image.name,re.IGNORECASE).groups()[1]
    unique_ID = str(uuid4())
    filepath = settings.TEMP_UPLOAD_DIR+unique_ID+ext
    with open(filepath,'wb+') as destination:
        destination.write(image.read())
    upload_image.delay(filepath,id,field,unique_ID,ext)

@login_required
def create_product(request):
    if request.method == 'POST':
        (verified,status,error) = format_product(request.POST,request.FILES)
        if status:
            new_entry = Producto(codigo = verified['codigo'],ean=verified['ean'],
                nombre=verified['nombre'],precio=verified['precio'],activo=verified['activo'],
                foto1=verified['foto1'],foto2=verified['foto2'],foto3=verified['foto3'],
                foto4=verified['foto4'],foto5=verified['foto5'],
                foto6=verified['foto6'],categoria=verified['categoria'],marca=verified['marca'],
                descripcion=request.POST['summernote'])
            new_entry.save()
            new_id = new_entry.id
            i=0
            for x in verified['images']:
                upload_image_task(x,new_id,i+1)
                i+=1
            messages.add_message(request,messages.SUCCESS,"Producto Creado",'alert-success')
            return redirect('products:index')
        else:
            messages.add_message(request,messages.ERROR,error,'alert-danger')
            return redirect('products:index')
    else:
        return redirect('products:index')

@login_required
def view_product(request,id_query):
    summernote = Summernote()
    product = get_object_or_404(Producto,id=id_query)
    marcas = Marca.objects.all()
    categorias = Categoria.objects.all()
    images = []
    for i in range(6):
        image = eval("product.foto{}".format(i+1))
        if image != "":
            images.append(image)
    return render(request, 'view_product.html',{'product':product,'marcas':marcas,'categorias':categorias,'images':images,'summernote':summernote})

@login_required
def delete_product(request,product_id=None):
    if request.method == 'POST':
        if product_id == None:
            raise Http404
        product = get_object_or_404(Producto,id=product_id)
        for x in range(6):
            if eval('product.foto{}'.format(x+1)) != '':
                match = re.match('^.*\/(.*)(\..*)$',eval('product.foto{}'.format(x+1))).groups()
                if match[0] != 'placeholder':
                    remove_image.delay(match[0],match[1])
        product.delete()
        print "Borrado producto id: " + product_id
        messages.add_message(request,messages.WARNING,product.nombre+" Borrado",'alert-warning')
        return redirect('products:index')
    else:
        return redirect('products:index')

def format_product(POST,FILES):
    (a,status,error) = verify_fields(POST)
    items = FILES.getlist('fotos')
    a['images'] = verify_post_files(items)
    if len(items) == 0:
        for x in range(6):
            a['foto{}'.format(x+1)] = ""
    else:
        i = 0
        for x in items:
            a['foto{}'.format(i+1)] = settings.PLACEHOLDER_URL
            i+=1
        for x in range(i,6):
            a['foto{}'.format(x+1)] = ""
    return (a,status,error)

def verify_fields(POST):
    status = True
    error = ''
    a = dict()
    try:
        if POST['nombre'] == "":
            status = False
            error = "Nombre Vacio"
            return (a,status,error)
        a['nombre'] = POST['nombre']
        if POST['codigo'] == "":
            status = False
            error = "Codigo vacio"
            return (a,status,error)
        a['codigo'] = POST['codigo']
        a['ean'] = POST['ean']
        if (POST['activo'] == '1' or POST['activo'] == '0'):
            a['activo'] = bool(int(POST['activo']))
        else:
            status = False
            error = "Campo 'Activo' invalido"
            return (a,status,error)
    except:
        status=False
        error = "Campos Invalidos"
        return (a,status,error)
    try:
        a['precio'] = float(POST['precio'])
        if a['precio'] == 0.0:
            status = False
            error = "Precio es Cero"
            return (a,status,error)
    except:
        status = False
        error = "Campo 'Precio' no es un numero valido"
        return (a,status,error)
    #try:
    #    a['cantidad'] = int(float(POST['cantidad']))
    #except:
    #    status = False
    #    error = "Campo 'Cantidad' no es un numero valido"
    try:
        if(POST['categoria'] != ''):
            a['categoria'] = Categoria.objects.get(id=int(POST['categoria']))
        else:
            a['categoria'] = None
    except:
        status = False
        error = "Categoria Invalida"
    try:
        if(POST['marca'] != ''):
            a['marca'] = Marca.objects.get(id=int(POST['marca']))
        else:
            a['marca'] = None
    except:
        status = False
        error = "Marca Invalida"
    return (a,status,error)

def verify_post_files(items):
    images = []
    for x in items:
        try:
            ext = re.match('(.*?)(\.[^.]*$|$)',x.name,re.IGNORECASE).groups()[1]
            if (ext == '.jpg' or ext == '.png' or ext == '.gif' or ext == '.jpeg'):
                if x.size <= settings.MAX_UPLOAD_SIZE:
                    images.append(x)
        except:
            pass
    return images

@login_required
def edit_product(request,id_query=None):
    if id_query is None:
        raise Http404
    product = get_object_or_404(Producto,id=id_query)
    if request.method == 'POST':
        (verified,status,error) = format_product(request.POST,request.FILES)
        if status:
            for x in range(6):
                try:
                    if eval("request.POST['borrar{}']".format(x+1)) == 'on':
                        match = re.match('^.*\/(.*)(\..*)$',eval('product.foto{}'.format(x+1))).groups()
                        if match[0] != "placeholder":
                            remove_image.delay(match[0],match[1])
                        exec("product.foto{} = ''".format(x+1))
                        print "Borrar " + match[0]
                except:
                    pass
            checked = False
            while not checked:
                for x in range(1,6):
                    if eval('product.foto{}'.format(x+1)) != '' and eval('product.foto{}'.format(x)) == '':
                        exec('product.foto{} = product.foto{}'.format(x,x+1))
                        exec('product.foto{} = \'\''.format(x+1))
                        break
                else:
                    checked = True
            for x in range(6):
                if eval('product.foto{}'.format(x+1)) == '':
                    try:
                        upload_image_task(verified['images'].pop(0),id_query,x+1)
                    except:
                        pass
            product.codigo = verified['codigo']
            product.ean = verified['ean']
            product.nombre = verified['nombre']
            product.categoria = verified['categoria']
            product.marca = verified['marca']
            product.precio = verified['precio']
            product.activo = verified['activo']
            product.save()
            messages.add_message(request,messages.SUCCESS,"Producto Editado",'alert-success')
            return redirect('product:index')
        else:
            messages.add_message(request,messages.ERROR,error,'alert-danger')

    else:
        id = id_query
        return redirect('products:index')

@login_required
def list_brands(request):
    marcas = Marca.objects.all()
    return render(request,'marcas.html',{'marcas':marcas})

@login_required
def create_brand(request):
    if request.method == 'POST':
        if request.POST['nombre'] == '':
            messages.add_message(request,messages.ERROR,"Nombre vacio","alert-danger")
            return redirect('products:list_brands')
        new = Marca(nombre = request.POST['nombre'])
        new.save()
        new_id = new.id
        images = verify_post_files(request.FILES.getlist('foto'))
        if len(images) > 0:
            image = images.pop(0)
            ext = re.match('(.*?)(\.[^.]*$|$)',image.name,re.IGNORECASE).groups()[1]
            unique_ID = str(uuid4())
            filepath = settings.TEMP_UPLOAD_DIR+unique_ID+ext
            with open(filepath,'wb+') as destination:
                destination.write(image.read())
            upload_image2.delay(filepath,new_id,unique_ID,ext)
        messages.add_message(request,messages.SUCCESS,"Marca creada",'alert-success')
        return redirect('products:list_brands')
    else:
        return redirect('products:list_brands')

@login_required
def view_brand(request,brand_id=None):
    if brand_id == None:
        raise Http404
    marca = get_object_or_404(Marca,id=brand_id)
    return render(request,'view_brand.html',{'marca':marca})

@login_required
def edit_brand(request,brand_id=None):
    if request.method == 'POST':
        if request.POST['nombre'] == '':
            messages.add_message(request,messages.ERROR,"Nombre vacio","alert-danger")
            return redirect('products:list_brands')
        if brand_id == None:
            raise Http404
        marca = get_object_or_404(Marca,id=brand_id)
        marca.nombre = request.POST['nombre']
        images = verify_post_files(request.FILES.getlist('imagen'))
        if request.POST['borrar'] == 'on':
            match = re.match('^.*\/(.*)(\..*)$',marca.imagen).groups()
            marca.imagen = ''
            remove_image.delay(match[0],match[1])
            if len(images) > 0:
                image = images.pop(0)
                ext = re.match('(.*?)(\.[^.]*$|$)',image.name,re.IGNORECASE).groups()[1]
                unique_ID = str(uuid4())
                filepath = settings.TEMP_UPLOAD_DIR+unique_ID+ext
                with open(filepath,'wb+') as destination:
                    destination.write(image.read())
                upload_image2.delay(filepath,brand_id,unique_ID,ext)
        elif marca.imagen == '' and len(images)>0:
            image = images.pop(0)     
            ext = re.match('(.*?)(\.[^.]*$|$)',image.name,re.IGNORECASE).groups()[1]
            unique_ID = str(uuid4())  
            filepath = settings.TEMP_UPLOAD_DIR+unique_ID+ext
            with open(filepath,'wb+') as destination:
                destination.write(image.read())
            upload_image2.delay(filepath,brand_id,unique_ID,ext)
        marca.save()
        messages.add_message(request,messages.SUCCESS,"Marca editada",'alert-success')
        return redirect('products:list_brands')
    else:
        return redirect('products:list_brands')

@login_required
def list_tags(request):
    categorias = Categoria.objects.all()
    return render(request,'categorias.html',{'categorias':categorias})

@login_required
def create_tag(request):
    if request.method == 'POST':
        if request.POST['nombre'] == '':
            messages.add_message(request,messages.ERROR,"Nombre vacio","alert-danger")
            return redirect('products:list_tags')
        if request.POST['categoria_padre'] == '':
            categoria_padre_edit = None
        else:
            try:
                categoria_padre_edit = Categoria.objects.get(id=int(request.POST['categoria_padre']))
            except:
                messages.add_message(request,messages.ERROR,"Categoria Padre Invalida","alert-danger")
                return redirect('product:list_tags')
        tag = Categoria(nombre=request.POST['nombre'],categoria_padre=categoria_padre_edit)
        tag.save()
        messages.add_message(request,messages.SUCCESS,"Categoria creada",'alert-success')
        return redirect('products:list_tags')

@login_required
def edit_tag(request,tag_id=None):
    if request.method == 'POST':
        if request.POST['nombre'] == '':
            messages.add_message(request,messages.ERROR,"Nombre vacio","alert-danger")
            return redirect('products:list_tags')
        if tag_id == None:
            raise Http404
        if request.POST['categoria_padre'] == '':
            categoria_padre_edit = None
        else:
            try:
                categoria_padre_edit = Categoria.objects.get(id=int(request.POST['categoria_padre']))
            except:
                messages.add_message(request,messages.ERROR,"Categoria Padre Invalida","alert-danger")
                return redirect('product:list_tags')
        tag = get_object_or_404(Categoria,id=tag_id)
        tag.nombre = request.POST['nombre']
        tag.categoria_padre = categoria_padre_edit
        tag.save()
        messages.add_message(request,messages.SUCCESS,"Categoria editada",'alert-success')
        return redirect('products:list_tags')
    else:
        return redirect('products:list_tags')

@login_required
def view_tag(request,tag_id=None):
    if tag_id == None:
        raise Http404
    tag = get_object_or_404(Categoria,id=tag_id)
    categorias = Categoria.objects.all()
    return render(request,'view_tag.html',{'tag':tag,'categorias':categorias})
