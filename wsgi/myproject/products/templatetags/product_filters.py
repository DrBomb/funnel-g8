from django.template import Library
from django.conf import settings
import re

register = Library()

@register.filter
def get_range( value ):
    """
    Filter - returns a list containing range made from given value
    Usage (in template):

    <ul>{% for i in 3|get_range %}
        <li>{{ i }}. Do something</li>
    {% endfor %}</ul>

    Results with the HTML:
    <ul>
        <li>0. Do something</li>
        <li>1. Do something</li>
        <li>2. Do something</li>
    </ul>

    Instead of 3 one may use the variable set in the views
    """
    return range( len(value) )


@register.filter
def sum(x,y):
    return x+y

@register.filter
def get_item_from_list(target_list, index):
    return (target_list[index])

@register.filter
def get_thumb(url):
    try:
        match = re.match('^.*\/(.*)(\..*)$',url).groups()
        return settings.AWS_S3["BASE_URL"]+"/"+settings.AWS_S3["THUMBNAILS_BUCKET"]+"/"+match[0]+".jpg"
    except:
        return ""

@register.filter
def id_string(var):
    return str(var)
