from __future__ import absolute_import

import os

from celery import Celery

if 'OPENSHIFT_REPO_DIR' in os.environ:
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "myproject.settings")
else:
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "myproject.development")

from django.conf import settings

app = Celery('myproject')

app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


@app.task(bind=True)
def debug_task(self):
        print('Request: {0!r}'.format(self.request))
