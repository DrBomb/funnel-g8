IntraG8 - Modulo Productos
==========================

Modulo de creacion, visualizacion y subida de imagenes.

Dependencias:

* Django
* Celery
* boto3
* python-resize-image
* django-summernote

Servicios externos

* CloudAMQP como sistema de mensajes para celery


Pasos para el deploy
-------------------

* Crear una app basada en Python 2.7
* Agregar cartucho de PostgreSQL
* Usando rhc configuramos las siguientes variables de entorno

```
rhc set-env -a <app> AWS_S3_IMAGES_BUCKET=<bucket>    	//Bucket de imagenes full resolucion
rhc set-env -a <app> AWS_S3_THUMBNAILS_BUCKET=<bucket>  //Bucket de thumbnails
rhc set-env -a <app> AWS_S3_BASE_URL=<URL>		//URL base para imagenes en los buckets
rhc set-env -a <app> AWS_S3_ACCESS_KEY=<KEY>		//Key de S3
rhc set-env -a <app> AWS_S3_SECRET_KEY=<SECRET>		//Secret de S3
rhc set-env -a <app> BROKER_URL=<URL>			//URL del broker que figura en CloudAMQP, con login y password incluido
```
Luego de agregadas estas variables se procede a subir el codigo al repositorio de openshift:
```
git clone https://DrBomb@bitbucket.org/DrBomb/funnel-g8.git
cd funnel-g8
git add remote openshift <ssh_de_openshift>
git push -f openshift master
```

Esperamos que termine el proceso de build, cuando termine creamos un superusuario:
```
cd $OPENSHIFT_REPO_DIR
cd wsgi/myproject
python manage.py createsuperuser
```
Y seguimos las instrucciones

La pagina no tiene index, pero las siguientes paginas estan disponibles:

* /products  	Para el modulo de productos
* /accounts 	Para el modulo de cuentas
* /admin 	Para el modulo de administracion de django



El siguiente texto son las instrucciones con las que venia el repo antes de comenzar el trabajo:

Django on OpenShift
===================

This git repository helps you get up and running quickly w/ a Django
installation on OpenShift.  The Django project name used in this repo
is 'myproject' but you can feel free to change it.  Right now the
backend is PostgreSQL.

On subsequent pushes, a `python manage.py makemigrations` and `python manage.py migrate` is executed to make
sure that any models you added are created in the DB.  If you do
anything that requires an alter table, you could add the alter
statements in `GIT_ROOT/.openshift/action_hooks/alter.sql` and then use
`GIT_ROOT/.openshift/action_hooks/deploy` to execute that script (make
sure to back up your database w/ `rhc app snapshot save` first :) )

You can also turn on the DEBUG mode for Django application using the
`rhc env set DEBUG=True --app APP_NAME`. If you do this, you'll get
nicely formatted error pages in browser for HTTP 500 errors.

Do not forget to turn this environment variable off and fully restart
the application when you finish:

```
$ rhc env unset DEBUG
$ rhc app stop && rhc app start
```

Running on OpenShift
--------------------

Create an account at https://www.openshift.com

Install the RHC client tools if you have not already done so:
    
    sudo gem install rhc
    rhc setup

Select the version of python (2.7 or 3.3) and create a application

    rhc app create django python-$VERSION

Add this upstream repo

    cd django
    git remote add upstream -m master git://github.com/openshift/django-example.git
    git pull -s recursive -X theirs upstream master

Then push the repo upstream

    git push

Now, you have to create [admin account](#admin-user-name-and-password), so you 
can setup your Django instance.
	
That's it. You can now checkout your application at:

    http://django-$yournamespace.rhcloud.com

Admin user name and password
----------------------------
Use `rhc ssh` to log into python gear and run this command:

	python $OPENSHIFT_REPO_DIR/wsgi/myproject/manage.py createsuperuser

You should be now able to login at:

	http://django-$yournamespace.rhcloud.com/admin/